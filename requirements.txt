asgiref==3.2.7
Django==3.0.7
django-bootstrap-form==3.4
django-widget-tweaks==1.4.8
python-decouple==3.3
pytz==2020.1
sqlparse==0.3.1

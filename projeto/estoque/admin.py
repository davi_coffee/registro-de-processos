from django.contrib import admin
from .models import Estoque, EstoqueItens


class EstoqueItensInline(admin.TabularInline):
    # Cria um modelo contendo as informações das tabela EstoqueItens (produto, quantidade,saldo)
    model = EstoqueItens
    extra = 0


@admin.register(Estoque)
class EstoqueAdmin(admin.ModelAdmin):
    inlines = (EstoqueItensInline, ) # Irá aparecer um nova tabela de add produtos com as informações de EstoqueItens
    list_display = (
        '__str__',
        'nf',
        'funcionario',
    )
    search_fields = ('nf',)
    list_filter = ('funcionario',)
    date_hierarchy = 'created' # irá aparecer as datas, dessa forma podendo filtrar

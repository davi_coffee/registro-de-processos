from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse_lazy
from projeto.core.models import TimeStampedModel
from projeto.produto.models import Produto


# Tupla dentro de tupla, para escolher o tipo de operação do estoque (s - saída, e -e entrada)
MOVIMENTO = (
    ('e', 'entrada'),
    ('s', 'saida'),
)


class Estoque(TimeStampedModel):
    """Cria uma tabela estoque, que contem esse campos com tais características"""
    funcionario = models.ForeignKey(User, on_delete=models.CASCADE)
    nf = models.PositiveIntegerField('nota fiscal', null=True, blank=True)
    movimento = models.CharField(max_length=1, choices=MOVIMENTO)

    class Meta: # o que não vai herdar
        """Ordering é uma lista em ordem decrescente a criação de tabelas, do app core classe TimeStampedModel"""
        ordering = ('-created', )

    def __str__(self):
        # Returna em string o ítem com indice pk
        return '{} - {} - {}'.format(self.pk, self.nf, self.created.strftime('%d-%m-%Y'))

    def get_absolute_url(self):
        return reverse_lazy('estoque:estoque_entrada_detail', kwargs={'pk': self.pk})

    def nf_formated(self):
        return str(self.nf).zfill(3) # coloca zero a esquerda com limete de 3 algarismo


class EstoqueItens(models.Model):
    """Cria uma tabela EstoqueItens, que vai conter os itens para comprar ou retirar."""
    estoque = models.ForeignKey(Estoque, on_delete=models.CASCADE, related_name='estoques') # pega da classe estoque
    produto = models.ForeignKey(Produto, on_delete=models.CASCADE) # pega da classe produto
    quantidade = models.PositiveIntegerField()
    saldo = models.PositiveIntegerField()

    class Meta:
        """Ordering é uma tupla que contém os índices dos campos."""
        ordering = ('pk',)

    def __str__(self):
        # retorna de mais compreensível o indice, o estoque desse indice, e o produto.
        return '{} - {} - {}'. format(self.pk, self.estoque.pk, self.produto)
from django.db import models

# Create your models here.


class TimeStampedModel(models.Model):
    """Cria uma tabela que contém as informações da de criação do produto e sua modificação"""
    created = models.DateTimeField(
        'criado em',
        auto_now_add=True,
        auto_now=False,
    )
    modified = models.DateTimeField(
        'modificado em',
        auto_now_add=False,
        auto_now=True,
    )

    class Meta:
        # A classe TimeStampedMode vai ser usada como classe pai em outro arquivo (true)
        abstract = True




from django.shortcuts import render
from django.views.generic import CreateView, UpdateView
from .models import Produto
from .forms import ProdutoForm


def produto_list(request):
    template_name = 'produto_list.html'
    objects = Produto.objects.all()
    context = {'object_list': objects} # Dict que possui os objetos da classe produto

    return render(request, template_name, context)


def produto_detail(request, pk):
    template_name = 'produto_detail.html'
    obj = Produto.objects.get(pk=pk)
    context = {'object': obj} # Dict que possui as descrições dos objetos da classe produto

    return render(request, template_name, context)


def produto_add(resquest):
    template_name = 'produto_form.html'
    return render(resquest, template_name)


class ProdutoCreate(CreateView):
    model = Produto
    template_name='produto_form.html'
    form_class= ProdutoForm


class ProdutoUpdate(UpdateView):
    model = Produto
    template_name='produto_form.html'
    form_class= ProdutoForm
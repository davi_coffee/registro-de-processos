from django.contrib import admin
from .models import Produto


#admin.site.register(Produto)

#Exibe na tela de administrador:
@admin.register(Produto)
class PrudutoAdmin(admin.ModelAdmin):
    list_display = ( # Campos que irá aparecer no display inicial da lista que contém todos
        '__str__',
        'importado',
        'ncm',
        'preco',
        'estoque',
        'estoque_minimo',
    )
    search_fields = ('produto',) # campo de busca na tela inicial da lista (busca pelos produtos)
    list_filter = ('importado',) # filtro na tela inicial da lista (filtra se é importado ou nao,ou todos)